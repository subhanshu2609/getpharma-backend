import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class PickupNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Pickup Not Found!", ApiErrorCode.PICKUP_NOT_FOUND);
  }
}
