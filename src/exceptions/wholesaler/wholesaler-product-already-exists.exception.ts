import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class WholesalerProductAlreadyExistsException extends ModelNotFoundException {

  constructor() {
    super("Wholesaler Product Already Exists Found!", ApiErrorCode.WHOLESALER_PRODUCT_NOT_FOUND);
  }
}
