import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class WholesalerProductNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Wholesaler Product Not Found!", ApiErrorCode.WHOLESALER_PRODUCT_NOT_FOUND);
  }
}
