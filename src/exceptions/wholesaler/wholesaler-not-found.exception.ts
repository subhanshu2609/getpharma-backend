import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class WholesalerNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Wholesaler Not Found!", ApiErrorCode.WHOLESALER_NOT_FOUND);
  }
}
