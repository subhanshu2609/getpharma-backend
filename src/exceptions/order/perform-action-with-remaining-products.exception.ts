import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class PerformActionWithRemainingProductsException extends ModelNotFoundException {

  constructor() {
    super("Perform Action With Remaining Products!", ApiErrorCode.PERFORM_ACTION_WITH_REMAINING_PRODUCTS);
  }
}
