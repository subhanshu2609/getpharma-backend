import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class OutOfStockException extends ModelNotFoundException {

  constructor() {
    super("Out Of Stock!", ApiErrorCode.OUT_OF_STOCK);
  }
}
