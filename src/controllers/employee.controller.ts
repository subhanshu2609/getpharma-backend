import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import { compareSync } from "bcrypt";
import { UserNotFoundException } from "../exceptions/user/user-not-found.exception";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import Ajv from "ajv";
import * as fs from "fs";
import { employeeService } from "../services/entities/employee.service";
import { EmployeeTransformer } from "../transformers/employee.transformer";
import { EmployeeCreateDto } from "../dtos/employee/employee-create.dto";
import { EmployeeUpdateDto } from "../dtos/employee/employee-update.dto";
import { orderService } from "../services/entities/order.service";
import { OrderTransformer } from "../transformers/order.transformer";
import { OrderNotFoundException } from "../exceptions/order/order-not-found.exception";
import { UnauthorizedException } from "../exceptions/root/unauthorized.exception";
import { EmployeeNotFoundException } from "../exceptions/user/employee-not-found.exception";
import { Order } from "../models/order.model";
import { OrderStatus, PickupStatus } from "../enums/order-status.enum";
import { IncorrectOtpException } from "../exceptions/user/incorrect-otp.exception";
import { historyService } from "../services/entities/history.service";
import { dbService } from "../services/db.service";
import { EmployeeCategory } from "../enums/employee-category.enum";
import { pickupService } from "../services/entities/pickup.service";
import { PickupNotFoundException } from "../exceptions/pickup/pickup-not-found.exception";
import { PickupTransformer } from "../transformers/pickup.transformer";
import { HistoryTransformer } from "../transformers/history.transformer";
import { PaymentStatus } from "../enums/payment-status.enum";
import { EmployeeHistoryIndexDto } from "../dtos/order/employee-history-index.dto";
import moment from "moment";
import { Stock } from "../models/stock.model";
import { StockStatus } from "../enums/stock-status.enum";
import { Employee } from "../models/employee.model";
import { inventoryService } from "../services/entities/inventory.service";
import { wholesalerService } from "../services/entities/wholesaler.service";
import { wholesalerProductService } from "../services/entities/wholesaler-product.service";
import { Product } from "../models/product.model";
import { productService } from "../services/entities/product.service";

export class EmployeeController {

  static async showEmployee(req: Request, res: Response) {
    const employeeId = +req.params.employeeId;
    const employee   = await employeeService.show(employeeId);

    if (!employee) {
      throw new UserNotFoundException();
    }

    return res.json({
      data: await new EmployeeTransformer().transform(employee)
    });
  }

  static async showPackagers(req: Request, res: Response) {
    const employees = await Employee.findAll({
      where: {
        category: EmployeeCategory.PACKAGER
      }
    });
    return res.json({
      data: await new EmployeeTransformer().transformList(employees)
    });
  }

  static async showDeliveryMan(req: Request, res: Response) {
    const employees = await Employee.findAll({
      where: {
        category: EmployeeCategory.DELIVERY_MAN
      }
    });
    return res.json({
      data: await new EmployeeTransformer().transformList(employees)
    });
  }


  static async showEmployees(req: Request, res: Response) {
    const employees = await Employee.findAll();
    return res.json({
      data: await new EmployeeTransformer().transformList(employees)
    });
  }

  static async login(req: Request, res: Response, next: NextFunction) {
    const inputData = req.body as { emailOrPhone: string; password: string; };
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/user/user-login.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    let employee;
    if (inputData.emailOrPhone.indexOf("@") !== -1) {
      employee = await employeeService.showEmployeeByEmail(inputData.emailOrPhone);
    } else {
      employee = await employeeService.showEmployeeByMobile(inputData.emailOrPhone);
    }
    if (!employee) {
      throw new EmployeeNotFoundException();
    }
    const isPasswordCorrect = compareSync(inputData.password, employee.password);

    if (!isPasswordCorrect) {
      res.status(403);
      return res.json({
        message: "Wrong Password"
      });
    }

    return res.json({
      token: jwt.sign({employee}, "secret"),
      data : await (new EmployeeTransformer()).transform(employee),
    });

  }


  static async createEmployee(req: Request, res: Response, next: NextFunction) {
    const inputData     = req.body as EmployeeCreateDto;
    const wholesalerIds = [];
    const ajv           = new Ajv();
    const schema        = JSON.parse(fs.readFileSync("./schema/employee/employee-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    if (req.employee.category != EmployeeCategory.ADMIN) {
      throw new UnauthorizedException("You are not authorized to perform this action", 301);
    }

    const employee = await employeeService.create(inputData);
    return res.json({
      data: await (new EmployeeTransformer()).transform(employee),
    });
  }

  static async myEmployeeProfile(req: Request, res: Response) {
    return res.json({
      employee: await (new EmployeeTransformer()).transform(req.employee),
    });
  }

  static async updateEmployee(req: Request, res: Response) {
    const employee = await employeeService.show(+req.params.employeeId);
    if (!employee) {
      throw new EmployeeNotFoundException();
    }
    const inputData = req.body as EmployeeUpdateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/employee/employee-update.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }

    const updatedEmployee = await employeeService.update(employee, inputData);

    return res.json({
      data: await (new EmployeeTransformer()).transform(updatedEmployee)
    });
  }

  static async updateMe(req: Request, res: Response) {
    const employee  = req.employee;
    const inputData = req.body as EmployeeUpdateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/employee/employee-update.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }

    const updatedEmployee = await employeeService.update(employee, inputData);

    return res.json({
      data: await (new EmployeeTransformer()).transform(updatedEmployee)
    });
  }

  static async deleteMe(req: Request, res: Response) {
    const employee = req.employee;
    await employeeService.delete(employee);
    return res.json({
      data: await new EmployeeTransformer().transform(employee)
    });
  }

  static async deleteById(req: Request, res: Response) {
    const employee = await employeeService.show(+req.params.employeeId);
    if (!employee) {
      throw new EmployeeNotFoundException();
    }
    await employeeService.delete(employee);
    return res.json("success");
  }

  static async deliveryOrders(req: Request, res: Response) {
    if (req.employee.category === EmployeeCategory.ADMIN) {
      const unassignedOrders = await Order.findAll({where: {delivery_man_id: null}});
      const assignedOrders   = await Order.findAll({where: {delivery_man_id: {$ne: null}}});
      return res.json({
        data: {
          unassignedOrders: await new OrderTransformer().transformList(unassignedOrders),
          assignedOrders  : await new OrderTransformer().transformList(assignedOrders)
        }
      });
    } else if (req.employee.category === EmployeeCategory.PACKAGER) {
      const orders = await orderService.showByPackager(req.employee.id);
      return res.json({
        data: await new OrderTransformer().transformList(orders),
      });
    } else {
      const orders = await orderService.showByDeliveryMan(req.employee.id);
      return res.json({
        data: await new OrderTransformer().transformList(orders),
      });
    }

  }

  static async assignEmployeeToOrder(req: Request, res: Response) {
    if (req.employee.category != EmployeeCategory.ADMIN) {
      throw new UnauthorizedException("You are not authorized to perform this action", 301);
    }
    const order = await orderService.showOrder(+req.params.orderId);
    if (!order) {
      throw new OrderNotFoundException();
    }
    req.body.delivery_man_id = +req.body.delivery_man_id;
    req.body.packager_id     = +req.body.packager_id;
    const inputData          = req.body as { packager_id: number, delivery_man_id: number };

    const packager     = await employeeService.show(inputData.packager_id);
    const delivery_man = await employeeService.show(inputData.delivery_man_id);

    if (!packager || !delivery_man) {
      throw new EmployeeNotFoundException();
    }
    const updatedOrder = await orderService.updateOrder(order, inputData);
    return res.json({
      data: await new OrderTransformer().transform(updatedOrder)
    });
  }

  static async assignPackager(req: Request, res: Response) {
    const pickup = await pickupService.show(+req.params.pickupId);
    if (!pickup) {
      throw new PickupNotFoundException();
    }
    const employee_id = +req.body.employee_id;
    if (!employee_id) {
      throw new UnauthorizedException("Kindly Choose an Employee", 560);
    }
    const employee = await employeeService.show(employee_id);
    if (!employee) {
      throw new EmployeeNotFoundException();
    }
    if (employee.category !== EmployeeCategory.PACKAGER) {
      throw new UnauthorizedException("Choose a packager", 561);
    }
    const updatedPickup = await pickup.update({
      employee_id  : employee_id,
      employee_name: employee.name
    });
    return res.json({
      data: await new PickupTransformer().transform(updatedPickup)
    });
  }

  static async deliverOrder(req: Request, res: Response) {
    const code = req.body.code;
    if (!code) {
      throw new UnprocessableEntityException("Code Not Found", "Sent code in body", 301);
    }
    const order = await orderService.showOrder(+req.body.order_id);
    if (!order) {
      throw new OrderNotFoundException();
    }
    if (order.delivery_code != code) {
      throw new IncorrectOtpException();
    }
    if (order.delivery_man_id != req.employee.id) {
      throw new UnauthorizedException("Not Your Order", 302);
    }
    const transaction = await dbService.getSequelize().transaction();
    try {
      const updatedOrder = await orderService.updateOrder(order, {
        order_status  : OrderStatus.DELIVERED,
        payment_status: PaymentStatus.SUCCESS
      }, transaction);
      const history      = await historyService.createHistory(updatedOrder, transaction);
      await orderService.deleteOrder(updatedOrder, transaction);
      await transaction.commit();
      return res.json("Delivered");
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }

  static async receivePickup(req: Request, res: Response) {
    const code = req.body.code;
    if (!code) {
      throw new UnprocessableEntityException("Code Not Found", "Sent code in body", 301);
    }
    const pickup = await pickupService.show(+req.params.pickupId);
    if (!pickup) {
      throw new PickupNotFoundException();
    }
    if (pickup.delivery_code != code) {
      throw new IncorrectOtpException();
    }
    if (pickup.employee_id != req.employee.id) {
      throw new UnauthorizedException("Not Your Order", 302);
    }
    const transaction = await dbService.getSequelize().transaction();
    try {
      const updatedPickup = await pickup.update({
        pickup_status: PickupStatus.SUCCESS,
        pickup_date  : moment().format("YYYY-MM-DD")
      }, {transaction});
      for (const product of updatedPickup.products) {
        const wp        = await wholesalerProductService.showByWholesalerAndProduct(updatedPickup.wholesaler_id, product.product_id);
        const p         = await productService.showProductById(product.product_id);
        const update    = await p.update({
          mrp: wp.mrp
        }, {transaction});
        const inventory = await inventoryService.createInventory(product.product_id, product.no_of_units, transaction);
      }
      await transaction.commit();
      return res.json({
        data: await new PickupTransformer().transform(updatedPickup)
      });
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }

  static async showHistory(req: Request, res: Response) {
    const filters = req.query as EmployeeHistoryIndexDto;
    let history;
    if (req.employee.category === EmployeeCategory.DELIVERY_MAN) {
      if (filters) {
        history = await historyService.showDeliveryManHistory(+req.employee.id, filters);
      } else {
        history = await historyService.showDeliveryManHistory(+req.employee.id);
      }
    } else if (req.employee.category === EmployeeCategory.PACKAGER) {
      if (filters) {
        history = await historyService.showPackagerHistory(+req.employee.id, filters);
      } else {
        history = await historyService.showPackagerHistory(+req.employee.id);
      }
    } else {
      if (filters) {
        history = await historyService.showAdminHistory(+req.employee.id, filters);
      } else {
        history = await historyService.showAdminHistory(+req.employee.id);
      }
    }
    return res.json({
      data: await new HistoryTransformer().transformList(history)
    });
  }

  static async deliveryManInvoice(req: Request, res: Response) {
    const filters = req.query as EmployeeHistoryIndexDto;
    let history;
    if (filters) {
      history = await historyService.showDeliveryManHistory(+req.employee.id, filters);
    } else {
      history = await historyService.showDeliveryManHistory(+req.employee.id);
    }
    const invoice = await historyService.deliveryManInvoice(history);
    return res.json({data: invoice});
  }

}
