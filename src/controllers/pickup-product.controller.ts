import { Request, Response } from "express";
import { PickupNotFoundException } from "../exceptions/pickup/pickup-not-found.exception";
import Ajv from "ajv";
import * as fs from "fs";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { pickupProductService } from "../services/entities/pickup-product.service";
import { PickupProductUpdateDto } from "../dtos/pickup-product/pickup-product-update.dto";
import { AvailabilityEnum } from "../enums/availability.enum";
import { PickupProductTransformer } from "../transformers/pickup-product.transformer";
import { pickupService } from "../services/entities/pickup.service";
import { dbService } from "../services/db.service";
import { PickupProduct } from "../models/pickup-product.model";
import { OrderStatus } from "../enums/order-status.enum";
import { UnauthorizedException } from "../exceptions/root/unauthorized.exception";
import { Helpers } from "../util/helpers.util";
import { DaysEnum } from "../enums/days.enum";
import { WholesalerProduct } from "../models/wholesaler-product.model";
import { wholesalerProductService } from "../services/entities/wholesaler-product.service";
import { WholesalerProductNotFoundException } from "../exceptions/wholesaler/wholesaler-product-not-found.exception";
import { WholesalerProductTransformer } from "../transformers/wholesaler-product.transformer";

export class PickupProductController {

  static async showPickupProducts(req: Request, res: Response) {
    const filters = req.query as { day?: DaysEnum };
    const pickups = await pickupProductService.showUpcoming(filters);
    return res.json({
      data: await new PickupProductTransformer().transformList(pickups)
    });
  }

  static async showUnassignedProducts(req: Request, res: Response) {
    const filters = req.query as { day?: DaysEnum };
    const pickups = await pickupProductService.showUnassigned(filters);
    return res.json({
      data: await new PickupProductTransformer().transformList(pickups)
    });
  }

  static async showWholesalerPickupProducts(req: Request, res: Response) {
    const pickups = await pickupProductService.showByWholesaler(+req.wholesaler.id);
    return res.json({
      data: await new PickupProductTransformer().transformList(pickups)
    });
  }

  static async indexPickupProducts(req: Request, res: Response) {
    const filters        = req.query as { status?: string; day?: string };
    const pickupProducts = await pickupProductService.indexPP(filters);
    return res.json({
      data: await new PickupProductTransformer().transformList(pickupProducts)
    });
  }

  static async updatePickupProduct(req: Request, res: Response) {
    if (req.body.available_quantity) {
      req.body.available_quantity = +req.body.available_quantity;
    }
    const inputData = req.body as PickupProductUpdateDto;
    if (inputData.new_price) {
      inputData.new_price = +inputData.new_price;
    }
    let pickupProduct = await pickupProductService.showPickupProduct(+req.params.pickupId);
    if (!pickupProduct) {
      throw new PickupNotFoundException();
    }
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/pickup-product/pickup-product-update.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e);
    }
    const transaction = await dbService.getSequelize().transaction();
    try {
      if (inputData.availability === AvailabilityEnum.COMPLETE) {
        inputData.available_quantity     = pickupProduct.required_quantity;
        pickupProduct.availability       = inputData.availability;
        pickupProduct.available_quantity = inputData.available_quantity;
        const nP                         = await pickupService.addProductToPickup(pickupProduct, transaction);
        await pickupProductService.deletePickupProduct(pickupProduct, transaction);
        await transaction.commit();
      } else if (inputData.availability === AvailabilityEnum.PARTIAL) {
        pickupProduct.availability       = inputData.availability;
        pickupProduct.available_quantity = inputData.available_quantity;
        if (pickupProduct.required_quantity < pickupProduct.available_quantity) {
          return Helpers.handleError(res, new UnauthorizedException("Quantity More Than Expected", 405));
        }
        const difference = pickupProduct.required_quantity - pickupProduct.available_quantity;
        if (difference !== 0) {
          await PickupProduct.create({
            product_id       : pickupProduct.product_id,
            required_quantity: difference,
            availability     : AvailabilityEnum.PENDING,
            order_status     : OrderStatus.PENDING,
            delivery_date    : pickupProduct.delivery_date,
            priority         : 1
          }, {transaction});
        }
        await pickupService.addProductToPickup(pickupProduct, transaction);
        await pickupProductService.deletePickupProduct(pickupProduct, transaction);
        await transaction.commit();
      } else if (inputData.availability === AvailabilityEnum.OUT_OF_STOCK) {
        inputData.amount             = null;
        inputData.available_quantity = null;
        pickupProduct                = await pickupProductService.updatePickupProduct(pickupProduct, inputData, transaction);
        await transaction.commit();
      } else {
        pickupProduct = await pickupProductService.updatePickupProduct(pickupProduct, inputData, transaction);
        if (inputData.availability === AvailabilityEnum.REVISED_RATE) {
          await pickupProduct.wholesalerProduct.update({
            deal_price : +inputData.new_price,
            is_assigned: false
          }, {transaction});
        }
        await transaction.commit();
      }
      return res.json({
        data: await new PickupProductTransformer().transform(pickupProduct)
      });
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }

  static async assignAllPickupsToWholesalers(req: Request, res: Response) {
    const pickupProducts = await pickupProductService.assignWholesalers();
    return res.json({
      data: await new PickupProductTransformer().transformList(pickupProducts)
    });
  }

  static async assignPickupToWholesalerByDay(req: Request, res: Response) {
    const inputData      = req.body as { day: DaysEnum };
    const pickupProducts = await pickupProductService.assignWholesalersByDay(inputData.day);
    return res.json({
      data: await new PickupProductTransformer().transformList(pickupProducts)
    });
  }

  static async assignPickupToWholesaler(req: Request, res: Response) {
    if (!req.params.pickupProductId || !req.body.wholesaler_product_id) {
      throw new UnprocessableEntityException("Incomplete Information");
    }
    const pickupProduct = await pickupProductService.showPickupProduct(+req.params.pickupProductId);
    if (!pickupProduct) {
      throw new PickupNotFoundException();
    }
    const wholesalerProduct = await wholesalerProductService.show(+req.body.wholesaler_product_id);
    if (!wholesalerProduct) {
      throw new WholesalerProductNotFoundException();
    }
    const update = await pickupProduct.update({
      wholesaler_product_id: +req.body.wholesaler_product_id,
      amount               : pickupProduct.required_quantity * wholesalerProduct.deal_price,
      priority             : pickupProduct.priority + 1,
      availability         : AvailabilityEnum.PENDING
    });

    return res.json({
      data: await new PickupProductTransformer().transform(update)
    });
  }

  static async actOnNewRate(req: Request, res: Response) {
    const inputData         = req.body as { action: string };
    const pickupProduct     = await pickupProductService.showPickupProduct(+req.params.pickupProductId);
    const wholesalerProduct = await WholesalerProduct.findById(pickupProduct.wholesaler_product_id);
    if (!pickupProduct || pickupProduct.availability != AvailabilityEnum.REVISED_RATE) {
      throw new PickupNotFoundException();
    }
    if (inputData.action === "accept") {
      await pickupService.addProductToPickup(pickupProduct);
      await pickupProductService.deletePickupProduct(pickupProduct);
      await wholesalerProduct.update({
        is_assigned   : true,
        previous_price: pickupProduct.wholesalerProduct.deal_price
      });
    } else if (inputData.action === "reject") {
      await pickupProduct.update({
        amount            : null,
        available_quantity: null,
        availability      : AvailabilityEnum.OUT_OF_STOCK
      });
      await wholesalerProduct.update({
        is_assigned   : false,
        previous_price: pickupProduct.wholesalerProduct.deal_price
      });
    }

    return res.json({
      data: await new PickupProductTransformer().transform(pickupProduct)
    });
  }

}
