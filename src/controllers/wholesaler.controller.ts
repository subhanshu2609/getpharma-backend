import { NextFunction, Request, Response } from "express";
import { WholesalerTransformer } from "../transformers/wholesaler.transformer";
import { WholesalerCreateDto } from "../dtos/wholesaler/wholesaler-create.dto";
import jwt from "jsonwebtoken";
import { WholesalerUpdateDto } from "../dtos/wholesaler/wholesaler-update.dto";
import { WholesalerNotFoundException } from "../exceptions/wholesaler/wholesaler-not-found.exception";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { cartService } from "../services/entities/cart.service";
import { dbService } from "../services/db.service";
import Ajv from "ajv";
import { compareSync } from "bcrypt";
import * as fs from "fs";
import { wholesalerService } from "../services/entities/wholesaler.service";
import { ProductIndexDto } from "../dtos/product/product-index.dto";
import { productService } from "../services/entities/product.service";
import { ProductTransformer } from "../transformers/product.transformer";
import toBoolean from "validator/lib/toBoolean";
import { User } from "../models/user.model";
import { UserTransformer } from "../transformers/user.transformer";
import { Wholesaler } from "../models/wholesaler.model";

export class WholesalerController {

  static async show(req: Request, res: Response) {
    const wholesalerId = +req.params.wholesalerId;
    const wholesaler   = await wholesalerService.show(wholesalerId);

    if (!wholesaler) {
      throw new WholesalerNotFoundException();
    }

    return res.json({
      data: await new WholesalerTransformer().transform(wholesaler)
    });
  }

  static async wholesalersForAdmin(req: Request, res: Response) {
    const filters     = req.query as ProductIndexDto;
    const wholesalers = await wholesalerService.wholesalersForAdmin(filters);
    return res.json({
      data: await new WholesalerTransformer().transformList(wholesalers)
    });
  }

  static async loginWholesaler(req: Request, res: Response) {
    const inputData = req.body as { emailOrPhone: string, password: string };
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/wholesaler/wholesaler-login.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e);
    }
    let wholesaler;
    if (inputData.emailOrPhone.indexOf("@") !== -1) {
      wholesaler = await wholesalerService.showWholesalerByEmail(inputData.emailOrPhone);
    } else {
      wholesaler = await wholesalerService.showWholesalerByMobile(inputData.emailOrPhone);
    }
    if (!wholesaler) {
      throw new WholesalerNotFoundException();
    }
    const isPasswordCorrect = compareSync(inputData.password, wholesaler.password);

    if (!isPasswordCorrect) {
      res.status(403);
      return res.json({
        message: "Wrong Password"
      });
    }
    return res.json({
      token: jwt.sign({wholesaler}, "secret"),
      data : await (new WholesalerTransformer()).transform(wholesaler),
    });
  }


  static async create(req: Request, res: Response, next: NextFunction) {
    const inputData = req.body as WholesalerCreateDto;

    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/wholesaler/wholesaler-create.schema.json").toString());
    const valid  = await ajv.validate(schema, inputData);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }
    const wholesaler = await wholesalerService.create(inputData);
    return res.json({
      data: await new WholesalerTransformer().transform(wholesaler),
    });
  }

  static async me(req: Request, res: Response) {
    const wholesaler = req.wholesaler;
    return res.json({
      data: await new WholesalerTransformer().transform(wholesaler)
    });
  }

  static async updateMe(req: Request, res: Response) {
    const wholesaler = req.wholesaler;
    const body       = req.body as WholesalerUpdateDto;
    const ajv        = new Ajv();
    const schema     = JSON.parse(fs.readFileSync("./schema/wholesaler/wholesaler-update.schema.json").toString());
    const valid      = await ajv.validate(schema, body);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }
    const updatedWholesaler = await wholesalerService.update(wholesaler, body);

    return res.json({
      token: jwt.sign({wholesaler}, "secret"),
      data : await (new WholesalerTransformer()).transform(updatedWholesaler)
    });
  }

  static async updateWholesaler(req: Request, res: Response) {
    const wholesaler = await wholesalerService.show(+req.params.wholesalerId);
    if (!wholesaler) {
      throw new WholesalerNotFoundException();
    }
    if (req.body.is_active) {
      req.body.is_active = toBoolean(req.body.is_active);
    }
    const body   = req.body as WholesalerUpdateDto;
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/wholesaler/wholesaler-update.schema.json").toString());
    const valid  = await ajv.validate(schema, body);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }
    const updatedWholesaler = await wholesalerService.update(wholesaler, body);

    return res.json({
      data: await (new WholesalerTransformer()).transform(updatedWholesaler)
    });
  }

  static async deleteMe(req: Request, res: Response) {
    const wholesaler = req.wholesaler;
    await wholesalerService.delete(wholesaler);
    return res.json("success");
  }

  static async delete(req: Request, res: Response) {
    const wholesaler = await wholesalerService.show(+req.params.wholesalerId);
    if (!wholesaler) {
      throw new WholesalerNotFoundException();
    }
    await wholesalerService.delete(wholesaler);
    return res.json("success");
  }

  static async showDeletedAt(req: Request, res: Response) {
    const users = await Wholesaler.findAll({paranoid: false, where: {deletedAt: {$ne: null}}});
    return res.json({
      data: await new WholesalerTransformer().transformList(users)
    });
  }

  static async restoreWholesaler(req: Request, res: Response) {
    const user = await Wholesaler.findOne({paranoid: false, where: {id: +req.params.userId}});
    user.setDataValue("deletedAt", null);
    const update = await user.save();
    return res.json({
      data: await new WholesalerTransformer().transform(update)
    });
  }
}
