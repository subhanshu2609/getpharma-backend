import { NextFunction, Request, Response } from "express";
import { userService } from "../services/entities/user.service";
import { UserTransformer } from "../transformers/user.transformer";
import { UserCreateDto } from "../dtos/user/user-create.dto";
import jwt from "jsonwebtoken";
import { UserUpdateDto } from "../dtos/user/user-update.dto";
import { UserNotFoundException } from "../exceptions/user/user-not-found.exception";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { cartService } from "../services/entities/cart.service";
import { dbService } from "../services/db.service";
import Ajv from "ajv";
import { compareSync } from "bcrypt";
import * as fs from "fs";
import { User } from "../models/user.model";

export class UserController {

  static async show(req: Request, res: Response) {
    const userId = +req.params.userId;
    const user   = await userService.show(userId);

    if (!user) {
      throw new UserNotFoundException();
    }

    return res.json({
      data: await new UserTransformer().transform(user)
    });
  }

  static async allUsers(req: Request, res: Response) {
    const users = await User.findAll();
    return res.json({
      data: await new UserTransformer().transformList(users)
    });
  }

  static async login(req: Request, res: Response) {
    const inputData = req.body as { emailOrPhone: string, password: string };
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/user/user-login.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e);
    }
    let user;
    if (inputData.emailOrPhone.indexOf("@") !== -1) {
      user = await userService.showUserByEmail(inputData.emailOrPhone);
    } else {
      user = await userService.showUserByMobile(inputData.emailOrPhone);
    }
    if (!user) {
      throw new UserNotFoundException();
    }
    const isPasswordCorrect = compareSync(inputData.password, user.password);

    if (!isPasswordCorrect) {
      res.status(403);
      return res.json({
        message: "Wrong Password"
      });
    }
    return res.json({
      token: jwt.sign({user}, "secret"),
      data : await (new UserTransformer()).transform(user),
    });
  }


  static async create(req: Request, res: Response, next: NextFunction) {
    if (req.body.delivery_charge) {
      req.body.delivery_charge = +req.body.delivery_charge;
    }
    const inputData             = req.body as UserCreateDto;
    inputData.permanent_address = inputData.address;

    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/user/user-create.schema.json").toString());
    const valid  = await ajv.validate(schema, inputData);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }
    const transaction = await dbService.getSequelize().transaction();
    try {
      const user = await userService.create(inputData, transaction);
      await cartService.createCart(user.id, user.delivery_charge, transaction);
      await transaction.commit();
      return res.json({
        data: await new UserTransformer().transform(user),
      });
    } catch (e) {
      await transaction.rollback();
      throw e;
    }
  }

  static async me(req: Request, res: Response) {
    const user = req.user;
    return res.json({
      data: await new UserTransformer().transform(user)
    });
  }

  static async updateMe(req: Request, res: Response) {
    const user   = req.user;
    const body   = req.body as UserUpdateDto;
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/user/user-update.schema.json").toString());
    const valid  = await ajv.validate(schema, body);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }
    const updatedUser = await userService.update(user, body);

    return res.json({
      token: jwt.sign({user}, "secret"),
      data : await (new UserTransformer()).transform(updatedUser)
    });
  }

  static async update(req: Request, res: Response) {
    const user = await userService.show(+req.params.userId);
    if (!user) {
      throw new UserNotFoundException();
    }
    if (req.body.delivery_charge) {
      req.body.delivery_charge = +req.body.delivery_charge;
    }
    const body   = req.body as UserUpdateDto;
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/user/user-update.schema.json").toString());
    const valid  = await ajv.validate(schema, body);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }
    const updatedUser = await userService.update(user, body);

    return res.json({
      token: jwt.sign({user}, "secret"),
      data : await (new UserTransformer()).transform(updatedUser)
    });
  }

  static async deleteMe(req: Request, res: Response) {
    const user = req.user;
    await userService.delete(user);
    return res.json("success");
  }

  static async delete(req: Request, res: Response) {
    const user = await userService.show(+req.params.userId);
    if (!user) {
      throw new UserNotFoundException();
    }
    await userService.delete(user);
    return res.json("success");
  }

  static async showDeletedAt(req: Request, res: Response) {
    const users = await User.findAll({paranoid: false, where: {deletedAt: {$ne: null}}});
    return res.json({
      data: await new UserTransformer().transformList(users)
    });
  }

  static async restoreRetailer(req: Request, res: Response) {
    const user = await User.findOne({paranoid: false, where: {id: +req.params.userId}});
    user.setDataValue("deletedAt", null);
    const update = await user.save();
    return res.json({
      data: await new UserTransformer().transform(update)
    });
  }

}
