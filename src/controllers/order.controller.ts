import { Request, Response } from "express";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { orderService } from "../services/entities/order.service";
import { OrderTransformer } from "../transformers/order.transformer";
import { OrderUpdateDto } from "../dtos/order/order-update.dto";
import { OrderNotFoundException } from "../exceptions/order/order-not-found.exception";
import { PaymentStatus } from "../enums/payment-status.enum";
import { historyService } from "../services/entities/history.service";
import { HistoryTransformer } from "../transformers/history.transformer";
import { OrderStatus } from "../enums/order-status.enum";
import Ajv from "ajv";
import * as fs from "fs";
import { dbService } from "../services/db.service";
import moment from "moment";
import { CannotCancelOrderException } from "../exceptions/order/cannot-cancel-order.exception";
import { AddressNotFoundException } from "../exceptions/address/address-not-found.exception";
import { cartService } from "../services/entities/cart.service";
import { pickupProductService } from "../services/entities/pickup-product.service";
import { productService } from "../services/entities/product.service";
import { ProductNotFoundException } from "../exceptions/product/product-not-found.exception";
import { UnauthorizedException } from "../exceptions/root/unauthorized.exception";
import { DaysEnum } from "../enums/days.enum";
import toBoolean from "validator/lib/toBoolean";
import { PerformActionWithRemainingProductsException } from "../exceptions/order/perform-action-with-remaining-products.exception";
import { PickupProduct } from "../models/pickup-product.model";

export class OrderController {

  static async showOrders(req: Request, res: Response) {
    const orders = await orderService.showIncompleteOrders(+req.user.id);
    return res.json({
      data: await new OrderTransformer().transformList(orders)
    });
  }

  static async showHistory(req: Request, res: Response) {
    const orders = await historyService.showHistory(+req.user.id);
    return res.json({
      data: await new HistoryTransformer().transformList(orders)
    });
  }

  static async addOrder(req: Request, res: Response) {
    const user = req.user;
    if (!user.address) {
      throw new AddressNotFoundException();
    }
    const transaction = await dbService.getSequelize().transaction();
    try {
      const cart   = await cartService.listCart(user.id, transaction);
      const order  = await orderService.addOrder(req.user, cart, transaction);
      const pickup = await pickupProductService.createPickupProducts(order, transaction);
      await transaction.commit();
      return res.json({
        data: await new OrderTransformer().transform(order)
      });
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }

  static async updateOrder(req: Request, res: Response) {
    if (req.body.employee_id) {
      req.body.employee_id = +req.body.employee_id;
    }
    const inputData = req.body as OrderUpdateDto;
    const orderId   = +req.params.orderId;
    const order     = await orderService.showOrder(orderId);
    if (!order) {
      throw new OrderNotFoundException();
    }
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/order/order-update.schema.json").toString());
    const valid  = await ajv.validate(schema, inputData);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }
    if (inputData.payment_status === PaymentStatus.FAILURE || inputData.order_status === OrderStatus.CANCELLED) {
      if (inputData.payment_status === PaymentStatus.FAILURE) {
        inputData.order_status = OrderStatus.REJECTED;
      }
      if (inputData.order_status === OrderStatus.CANCELLED) {
        if (moment() >= moment(order.expected_date).subtract(1, "h")) {
          console.log(moment().format());
          console.log(moment(order.expected_date).subtract(1, "h").format());
          throw new CannotCancelOrderException();
        }
        inputData.payment_status = PaymentStatus.PENDING;
      }
      const transaction = await dbService.getSequelize().transaction();
      try {
        const updatedOrder = await orderService.updateOrder(order, inputData, transaction);
        const history      = await historyService.createHistory(updatedOrder, transaction);
        console.log(history);
        await orderService.deleteOrder(updatedOrder, transaction);
        await transaction.commit();
        return res.json({
          data: await new HistoryTransformer().transform(history)
        });
      } catch (e) {
        await transaction.rollback();
        throw new UnprocessableEntityException(e);
      }
    }
    const updatedOrder = await orderService.updateOrder(order, inputData);
    return res.json({
      data: await new OrderTransformer().transform(updatedOrder)
    });
  }

  static async addPendingProducts(req: Request, res: Response) {
    req.body.product_id = +req.body.product_id;
    if (req.body.no_of_units) {
      req.body.no_of_units = +req.body.no_of_units;
    }
    req.body.available = toBoolean(req.body.available);
    const inputData    = req.body as { available: boolean; product_id: number; no_of_units?: number };
    const order        = await orderService.showOrder(+req.params.orderId);
    if (!order) {
      throw new OrderNotFoundException();
    }
    const product = await productService.showProductById(inputData.product_id);
    if (!product) {
      throw new ProductNotFoundException();
    }
    if (+req.employee.id !== order.packager_id) {
      throw new UnauthorizedException("You cannot perform this action", 301);
    }
    const updatedOrder = await orderService.changeStatus(order, inputData);
    return res.json({
      data: await new OrderTransformer().transform(updatedOrder)
    });
  }

  static async cancelOrder(req: Request, res: Response) {
    const order = await orderService.showOrder(+req.params.orderId);
    if (!order) {
      throw new OrderNotFoundException();
    }
    order.order_status = OrderStatus.CANCELLED;
    const transaction  = await dbService.getSequelize().transaction();
    try {
      for (const product of order.products) {
        const pickup_product = await PickupProduct.findOne({
          where: {
            product_id   : product.product_id,
            delivery_date: order.expected_date
          }
        });
        const new_quantity   = pickup_product.required_quantity - product.no_of_units;
        if (new_quantity <= 0) {
          await pickup_product.destroy({transaction});
        } else {
          await pickup_product.update({
            required_quantity: new_quantity,
            amount           : pickup_product.amount - (product.rate * product.no_of_units)
          }, {transaction});
        }
      }
      const history = await historyService.createHistory(order, transaction);
      await order.destroy({transaction});
      await transaction.commit();
      return res.json({
        data: await new HistoryTransformer().transform(history)
      });
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }

  static async packOrder(req: Request, res: Response) {
    const order = await orderService.showOrder(+req.params.orderId);
    if (!order) {
      throw new OrderNotFoundException();
    }
    if (order.packager_id !== req.employee.id) {
      throw new UnauthorizedException("Not Your Order", 321);
    }
    if (order.products.length > 0) {
      throw new PerformActionWithRemainingProductsException();
    }

    const transaction = await dbService.getSequelize().transaction();
    try {
      const update = await order.update({
        order_status: OrderStatus.PACKED
      }, {transaction});
      if (update.pending_products.length > 0) {
        let amount = 0;
        for (const product of update.pending_products) {
          amount = amount + (product.rate * product.no_of_units);
        }
        const cart     = {
          id             : update.cart_id,
          user_id        : update.user_id,
          products       : update.pending_products,
          amount         : amount,
          delivery_charge: 0,
          user           : update.user
        };
        const address  = update.delivery_address;
        const newOrder = await orderService.createOrder(update.user, cart, address, transaction);
      }
      await transaction.commit();
      return res.json({
        data: await new OrderTransformer().transform(update)
      });
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }

  static async historyByStatus(req: Request, res: Response) {
    const filter = req.query as { day?: string };
    const orders = await historyService.indexHistory(filter);
    return res.json({
      something: orders.map(o => o.delivery_date),
      data     : await new HistoryTransformer().transformList(orders)
    });
  }

  static async orderByStatus(req: Request, res: Response) {
    const filter = req.query as { status?: OrderStatus; day?: DaysEnum };
    const orders = await orderService.indexOrders(filter);
    return res.json({
      something: orders.map(o => o.expected_date),
      data     : await new OrderTransformer().transformList(orders)
    });
  }

  static async productCount(req: Request, res: Response) {
    const products = await historyService.productSold();
    return res.json({data: products});
  }

}
