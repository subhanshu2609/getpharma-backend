import { Request, Response } from "express";
import { Stock } from "../models/stock.model";
import { Product } from "../models/product.model";
import { Order } from "../models/order.model";
import { UnauthorizedException } from "../exceptions/root/unauthorized.exception";
import { Inventory } from "../models/inventory.model";
import { productService } from "../services/entities/product.service";
import { ProductNotFoundException } from "../exceptions/product/product-not-found.exception";
import { inventoryService } from "../services/entities/inventory.service";

export class StockController {

  static async createStock(req: Request, res: Response) {
    req.body.product_id  = +req.body.product_id;
    req.body.no_of_units = +req.body.no_of_units;
    const inputData      = req.body as { product_id: number; no_of_units: number };
    const stock          = Stock.create(inputData);
    return res.json({
      data: stock
    });
  }

  static async indexStock(req: Request, res: Response) {
    const stock = await Stock.findAll({include: [Product]});
    return res.json({
      data: stock
    });
  }

  static async updateStock(req: Request, res: Response) {
    const inputData = req.body as { no_of_units: number };
    const stock     = await Stock.update({
      no_of_units: inputData.no_of_units
    }, {
      where: {
        id: +req.params.stockId
      }
    });
    if (!stock) {
      throw new UnauthorizedException("Stock Not Found", 701);
    }
    return res.json({
      data: stock
    });
  }

  static async deleteStock(req: Request, res: Response) {
    const stock = await Stock.findOne({
      where: {
        id: +req.params.stockId
      }
    });
    if (!stock) {
      throw new UnauthorizedException("Stock Not Found", 701);
    }
    await stock.destroy();
    return res.json("success");
  }

  // INVENTORIES

  static async viewInventory(req: Request, res: Response) {
    const inventory = await Inventory.findAll({include: [Product]});
    return res.json({
      data: inventory
    });
  }

  static async reduceFromInventory(req: Request, res: Response) {
    req.body.product_id  = +req.body.product_id;
    req.body.no_of_units = +req.body.no_of_units;
    const inputData      = req.body as { product_id: number; no_of_units: number };
    const product        = await productService.showProductById(inputData.product_id);
    if (!product) {
      throw new ProductNotFoundException();
    }
    const inventory = await inventoryService.reduceQty(inputData.product_id, inputData.no_of_units);
    return res.json({
      data: inventory
    });
  }


}
