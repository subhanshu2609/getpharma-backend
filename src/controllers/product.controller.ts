import { Request, Response } from "express";
import { productService } from "../services/entities/product.service";
import { ProductCategoryTransformer } from "../transformers/product-category.transformer";
import { ProductTransformer } from "../transformers/product.transformer";
import { ProductCategoryNotFoundException } from "../exceptions/product/product-category-not-found.exception";
import { ProductNotFoundException } from "../exceptions/product/product-not-found.exception";
import * as fs from "fs";
import { BulkCreateDto, ProductCreateDto } from "../dtos/product/product-create.dto";
import { ProductUpdateDto } from "../dtos/product/product-update.dto";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { ENV_S3_BASE_URL } from "../util/secrets.util";
import { ProductIndexDto } from "../dtos/product/product-index.dto";
import Ajv from "ajv";
import toBoolean from "validator/lib/toBoolean";
import { UserTransformer } from "../transformers/user.transformer";
import { Product } from "../models/product.model";
import { ImageUploadType } from "../enums/image-upload-type.enum";
import { s3Service } from "../services/factories/s3.service";
import { EmployeeCategory } from "../enums/employee-category.enum";
import { wholesalerProductService } from "../services/entities/wholesaler-product.service";

export class ProductController {

  static async listProductCategories(req: Request, res: Response) {
    const categories = await productService.listCategories();
    return res.json({
      data: await new ProductCategoryTransformer().transformList(categories)
    });
  }

  static async categoriesForAdmin(req: Request, res: Response) {
    const categories = await productService.allCategories();
    return res.json({
      data: await new ProductCategoryTransformer().transformList(categories)
    });

  }

  static async listProducts(req: Request, res: Response) {
    const categoryId = +req.params.categoryId;
    const filters    = req.query as ProductIndexDto;
    if (filters.offset) {
      filters.offset = +filters.offset;
    }
    if (filters.limit) {
      filters.limit = +filters.limit;
    }
    const products = await productService.listProducts(filters, categoryId);
    return res.json({
      data: await new ProductTransformer().transformList(products)
    });
  }

  static async countProducts(req: Request, res: Response) {
    const count = await Product.count();
    return res.json({
      data: {
        count
      }
    });
  }

  static async allProducts(req: Request, res: Response) {
    const filters = req.query as ProductIndexDto;
    if (filters.offset) {
      filters.offset = +filters.offset;
    }
    if (filters.limit) {
      filters.limit = +filters.limit;
    }
    const products = await productService.allProducts(filters);
    return res.json({
      data: await new ProductTransformer().transformList(products)
    });
  }


  static async productsForAdmin(req: Request, res: Response) {
    const filters = req.query as ProductIndexDto;
    if (filters.offset) {
      filters.offset = +filters.offset;
    }
    if (filters.limit) {
      filters.limit = +filters.limit;
    }
    const products = await productService.productsForAdmin(filters);
    return res.json({
      data: await new ProductTransformer().transformList(products)
    });
  }

  static async trendingProducts(req: Request, res: Response) {
    const products = await productService.trendingProducts();
    return res.json({
      data: await new ProductTransformer().transformList(products)
    });
  }

  static async wholesalerTrends(req: Request, res: Response) {
    const wp = await wholesalerProductService.showByWholesalerId(+req.wholesaler.id);
    const category = await productService.showCategory(+req.params.categoryId);
    if (!category) {
      throw new ProductCategoryNotFoundException();
    }
    const products = await productService.wholesalerTrends(category.id, wp.map(p => p.product_id));
    return res.json({
      data: await new ProductTransformer().transformList(products)
    });
  }

  static async createProductCategory(req: Request, res: Response) {
    const inputData = req.body as { title: string; type: ImageUploadType };
    const image     = req.file;
    let uploadUrl   = "";
    if (image) {
      await s3Service.uploadToS3(image.buffer, image.originalname, inputData.type);
      uploadUrl = ENV_S3_BASE_URL + inputData.type + "/" + image.originalname;
    }
    const product = await productService.addCategory(inputData.title, uploadUrl);
    return res.json({
      data: await new ProductCategoryTransformer().transform(product)
    });
  }

  static async createProduct(req: Request, res: Response) {
    req.body.category_id = +req.body.category_id;
    req.body.mrp         = +req.body.mrp;
    if (req.body.off_percentage) {
      req.body.off_percentage = +req.body.off_percentage;
    }
    if (req.body.off_amount) {
      req.body.off_amount = +req.body.off_amount;
    }
    if (req.body.selling_price) {
      req.body.selling_price = +req.body.selling_price;
    }
    if (req.body.is_trending) {
      req.body.is_trending = toBoolean(req.body.is_trending);
    } else {
      req.body.is_trending = false;
    }
    if (req.body.is_active) {
      req.body.is_active = toBoolean(req.body.is_active);
    }
    const inputData = req.body as ProductCreateDto;
    const image     = req.file;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/product/product-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    let uploadUrl = "";
    if (image) {
      await s3Service.uploadToS3(image.buffer, image.originalname, inputData.type);
      uploadUrl = ENV_S3_BASE_URL + inputData.type + "/" + image.originalname;
    }
    const product = await productService.addProduct(inputData, uploadUrl);
    return res.json({
      data: await new ProductTransformer().transform(product)
    });
  }

  static async bulkCreateProduct(req: Request, res: Response) {
    const inputData = req.body as BulkCreateDto[];
    for (const product of inputData) {
      await productService.bulkCreateProduct(product);
    }
    return res.json("success");
  }

  static async updateProductCategory(req: Request, res: Response) {
    const categoryId = +req.params.categoryId;
    const inputData  = req.body as { title: string; type: ImageUploadType, is_active: boolean };
    const image      = req.file;
    const category   = await productService.showCategory(categoryId);
    if (!category) {
      throw new ProductCategoryNotFoundException();
    }
    let uploadUrl = category.image_url;
    if (image) {
      await s3Service.uploadToS3(image.buffer, image.originalname, inputData.type);
      uploadUrl = ENV_S3_BASE_URL + inputData.type + "/" + image.originalname;
    }
    if (inputData.is_active === true || inputData.is_active === false) {
    } else {
      inputData.is_active = category.is_active;
    }
    const update = await productService.updateCategory(category, inputData.title, inputData.is_active, uploadUrl);
    return res.json({
      data: await new ProductCategoryTransformer().transform(update)
    });
  }

  static async updateProduct(req: Request, res: Response) {
    if (req.body.category_id) {
      req.body.category_id = +req.body.category_id;
    }
    if (req.body.mrp) {
      req.body.mrp = +req.body.mrp;
    }
    if (req.body.off_percentage) {
      req.body.off_percentage = +req.body.off_percentage;
    }
    if (req.body.off_amount) {
      req.body.off_amount = +req.body.off_amount;
    }
    if (req.body.selling_price) {
      req.body.selling_price = +req.body.selling_price;
    }
    if (req.body.is_active && typeof req.body.is_active != "boolean") {
      req.body.is_active = toBoolean(req.body.is_active);
    }
    if (req.body.is_trending && typeof req.body.is_trending != "boolean") {
      req.body.is_trending = toBoolean(req.body.is_trending);
    }
    const productId = +req.params.productId;
    const inputData = req.body as ProductUpdateDto;
    const product   = await productService.showProductById(productId);
    if (!product) {
      throw new ProductNotFoundException();
    }
    const image  = req.file;
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/product/product-update.schema.json").toString());
    const valid  = await ajv.validate(schema, inputData);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }
    let uploadUrl = product.image_url;
    if (image) {
      await s3Service.uploadToS3(image.buffer, image.originalname, inputData.type);
      uploadUrl = ENV_S3_BASE_URL + inputData.type + "/" + image.originalname;
    }
    const update = await productService.updateProduct(product, inputData, uploadUrl);

    return res.json({
      data: await new ProductTransformer().transform(update)
    });
  }

  static async deleteProductCategory(req: Request, res: Response) {
    const categoryId = +req.params.categoryId;

    const category = await productService.showCategory(categoryId);
    if (!category) {
      throw new ProductCategoryNotFoundException();
    }
    await productService.deleteCategory(category);
    return res.json("Success");
  }

  static async deleteProduct(req: Request, res: Response) {
    const productId = +req.params.productId;

    const product = await productService.showProductById(productId);
    if (!product) {
      throw new ProductNotFoundException();
    }
    await productService.deleteProduct(product);
    return res.json("Success");
  }

  static async addToFavorites(req: Request, res: Response) {
    const productId = +req.params.productId;
    const product   = await productService.showProductById(productId);
    if (!product) {
      throw new ProductNotFoundException();
    }
    const user      = req.user;
    const favorites = user.favorites;
    favorites.push(productId);
    const distinct    = favorites.filter((value, index, self) => self.indexOf(value) === index);
    const updatedUser = await req.user.update({
      favorites: distinct
    });
    return res.json({
      data: await new UserTransformer().transform(user)
    });
  }

  static async removeFromFavorites(req: Request, res: Response) {
    const productId = +req.params.productId;
    const favorites = req.user.favorites;
    let updatedUser = req.user;
    const index     = favorites.indexOf(productId);
    console.log(index);
    if (index > -1) {
      console.log("hello");
      favorites.splice(favorites.indexOf(productId), 1);
      updatedUser = await req.user.update({
        favorites: favorites
      });
    }
    return res.json({
      data: await new UserTransformer().transform(updatedUser)
    });
  }

  static async emptyFavorites(req: Request, res: Response) {
    await req.user.update({
      favorites: []
    });
    return res.json({
      data: await new UserTransformer().transform(req.user)
    });
  }

  static async showFavorites(req: Request, res: Response) {
    const products = await Product.findAll({
      where: {
        id: req.user.favorites
      }
    });
    return res.json({
      data: await new ProductTransformer().transformList(products)
    });
  }

}
