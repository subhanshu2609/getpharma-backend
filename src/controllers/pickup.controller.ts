import { Request, Response } from "express";
import { pickupService } from "../services/entities/pickup.service";
import { PickupTransformer } from "../transformers/pickup.transformer";
import { DaysEnum } from "../enums/days.enum";
import { PickupStatus } from "../enums/order-status.enum";

export class PickupController {

  static async showPickup(req: Request, res: Response) {
    const pickup = await pickupService.show(+req.params.pickupId);
    return res.json({
      data: await new PickupTransformer().transform(pickup)
    });
  }

  static async showMyInvoice(req: Request, res: Response) {
    const invoice = await pickupService.showUpcomingPickup(+req.wholesaler.id);
    return res.json({
      data: await new PickupTransformer().transform(invoice)
    });
  }

  static async indexPickups(req: Request, res: Response) {
    const filters = req.query as { status?: PickupStatus };
    const pickups = await pickupService.indexPickups(filters);
    return res.json({
      data: await new PickupTransformer().transformList(pickups)
    });
  }

  static async showWholesalerPickups(req: Request, res: Response) {
    const pickups = await pickupService.showPickupByWholesaler(+req.wholesaler.id);
    return res.json({
      data: await new PickupTransformer().transformList(pickups)
    });
  }

  static async showAcceptedWholesalerPickups(req: Request, res: Response) {
    const pickups = await pickupService.showAcceptedPickupByWholesaler(+req.wholesaler.id);
    return res.json({
      data: await new PickupTransformer().transformList(pickups)
    });
  }

  static async showEmployeePickups(req: Request, res: Response) {
    const pickups = await pickupService.showPickupByEmployee(+req.employee.id);
    return res.json({
      data: await new PickupTransformer().transformList(pickups)
    });
  }

  static async packagerInvoice(req: Request, res: Response) {
    const pickups = await pickupService.showPackagerInvoice(+req.employee.id);
    return res.json({
      data: pickups
    });
  }

}
