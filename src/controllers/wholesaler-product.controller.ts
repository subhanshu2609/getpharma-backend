import { NextFunction, Request, Response } from "express";
import { wholesalerProductService } from "../services/entities/wholesaler-product.service";
import { WholesalerProductNotFoundException } from "../exceptions/wholesaler/wholesaler-product-not-found.exception";
import { WholesalerProductTransformer } from "../transformers/wholesaler-product.transformer";
import { wholesalerService } from "../services/entities/wholesaler.service";
import { WholesalerNotFoundException } from "../exceptions/wholesaler/wholesaler-not-found.exception";
import { productService } from "../services/entities/product.service";
import { ProductNotFoundException } from "../exceptions/product/product-not-found.exception";
import { WholesalerProductUpdateDto } from "../dtos/wholesaler-product/wholesaler-product-update.dto";
import toBoolean from "validator/lib/toBoolean";
import Ajv from "ajv";
import * as fs from "fs";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { WholesalerProductCreateDto } from "../dtos/wholesaler-product/wholesaler-product-create.dto";
import { ProductWholesalerProductCreateDto } from "../dtos/wholesaler-product/product-wholesaler-product-create.dto";
import { dbService } from "../services/db.service";
import { WholesalerProduct } from "../models/wholesaler-product.model";
import { WholesalerProductAlreadyExistsException } from "../exceptions/wholesaler/wholesaler-product-already-exists.exception";

export class WholesalerProductController {

  static async show(req: Request, res: Response) {
    const wholesalerProduct = await wholesalerProductService.show(+req.params.wpId);
    if (!wholesalerProduct) {
      throw new WholesalerProductNotFoundException();
    }
    return res.json({
      data: await new WholesalerProductTransformer().transform(wholesalerProduct)
    });
  }

  static async showAssignedWP(req: Request, res: Response) {
    const wholesalerProducts = await wholesalerProductService.showAssignedWP();
    return res.json({
      data: await new WholesalerProductTransformer().transformList(wholesalerProducts)
    });
  }

  static async showByWholesaler(req: Request, res: Response) {
    const wholesaler = await wholesalerService.show(+req.params.wholesalerId);
    if (!wholesaler) {
      throw new WholesalerNotFoundException();
    }
    const wholesalerProducts = await wholesalerProductService.showByWholesalerId(wholesaler.id);
    return res.json({
      data: await new WholesalerProductTransformer().transformList(wholesalerProducts)
    });
  }

  static async showByProduct(req: Request, res: Response) {
    const product = await productService.showProductById(+req.params.productId);
    if (!product) {
      throw new ProductNotFoundException();
    }
    const wholesalerProducts = await wholesalerProductService.showByProductId(product.id);
    return res.json({
      data: await new WholesalerProductTransformer().transformList(wholesalerProducts)
    });
  }

  static async createWP(req: Request, res: Response) {
    if (req.body.off_percentage) {
      req.body.off_percentage = +req.body.off_percentage;
    }
    if (req.body.off_amount) {
      req.body.off_amount = +req.body.off_amount;
    }
    if (req.body.deal_price) {
      req.body.deal_price = +req.body.deal_price;
    }
    req.body.product_id    = +req.body.product_id;
    req.body.wholesaler_id = +req.body.wholesaler_id;
    const inputData        = req.body as WholesalerProductCreateDto;

    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/wholesaler-product/wholesaler-product-create.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e);
    }

    const wholesaler = await wholesalerService.show(inputData.wholesaler_id);
    if (!wholesaler) {
      throw new WholesalerNotFoundException();
    }

    const product = await productService.showProductById(inputData.product_id);
    if (!product) {
      throw new ProductNotFoundException();
    }

    const wp = await WholesalerProduct.findOne({
      where: {
        wholesaler_id: wholesaler.id,
        product_id   : product.id
      }
    });
    if (wp) {
      throw new WholesalerProductAlreadyExistsException();
    }

    if (req.wholesaler) {
      inputData.is_assigned = false;
    } else if (req.employee) {
      inputData.is_assigned = true;
    }

    const create = await wholesalerProductService.create(inputData, product);
    return res.json({
      data: await new WholesalerProductTransformer().transform(create)
    });
  }

  static async createProductByWholesaler(req: Request, res: Response) {
    if (req.body.off_percentage) {
      req.body.off_percentage = +req.body.off_percentage;
    }
    if (req.body.off_amount) {
      req.body.off_amount = +req.body.off_amount;
    }
    if (req.body.deal_price) {
      req.body.deal_price = +req.body.deal_price;
    }
    req.body.category_id = +req.body.category_id;
    req.body.mrp         = +req.body.mrp;
    const inputData      = req.body as ProductWholesalerProductCreateDto;

    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/wholesaler-product/product-wholesaler-product-create.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e);
    }

    const wholesaler = await wholesalerService.show(+req.wholesaler.id);
    if (!wholesaler) {
      throw new WholesalerNotFoundException();
    }
    const transaction = await dbService.getSequelize().transaction();
    let create;
    try {
      create = await wholesalerProductService.createProductForWP(inputData, wholesaler.id, transaction);
      await transaction.commit();
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
    return res.json({
      data: await new WholesalerProductTransformer().transform(create)
    });
  }

  static async updateWP(req: Request, res: Response) {
    if (req.body.mrp) {
      req.body.mrp = +req.body.mrp;
    }
    if (req.body.off_percentage) {
      req.body.off_percentage = +req.body.off_percentage;
    }
    if (req.body.off_amount) {
      req.body.off_amount = +req.body.off_amount;
    }
    if (req.body.deal_price) {
      req.body.deal_price = +req.body.deal_price;
    }
    if (req.body.is_assigned && typeof req.body.is_assigned != "boolean") {
      req.body.is_assigned = toBoolean(req.body.is_assigned);
    }
    const inputData         = req.body as WholesalerProductUpdateDto;
    const wholesalerProduct = await wholesalerProductService.show(+req.params.wpId);
    if (!wholesalerProduct) {
      throw new WholesalerProductNotFoundException();
    }

    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/wholesaler-product/wholesaler-product-update.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e);
    }

    let update;
    if (req.wholesaler) {
      if (inputData.deal_price) {
        inputData.is_assigned = false;
      }
      update                   = await wholesalerProductService.update(wholesalerProduct, inputData);
    } else {
      update = await wholesalerProductService.update(wholesalerProduct, inputData, true);
    }

    return res.json({
      data: await new WholesalerProductTransformer().transform(update)
    });
  }

  static async deleteWP(req: Request, res: Response) {
    const wholesalerProduct = await wholesalerProductService.show(+req.params.wpId);
    if (!wholesalerProduct) {
      throw new WholesalerProductNotFoundException();
    }
    await wholesalerProductService.delete(wholesalerProduct);
    return res.json("success");
  }
}
